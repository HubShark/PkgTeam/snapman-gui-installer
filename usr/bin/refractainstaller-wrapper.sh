#!/usr/bin/env bash
#
# New wrapper for refractainstaller-uefi/yad 9.2.2

TEXTDOMAIN=refractainstaller-gui
TEXTDOMAINDIR=/usr/share/locale/

if ! [[ -f /usr/bin/yad ]]; then
	xterm -hold -fa mono -fs 14 -geometry 80x20+0+0 -e echo $"
  Yad is not installed, Install it or run 'refractainstaller' from a 
  terminal or console for the CLI version.
  " &
fi


if [[ -d /sys/firmware/efi ]]; then
	installer="/usr/bin/refractainstaller-uefi -d"
else
	installer="/usr/bin/refractainstaller-yad -d"
fi


yad --question --title=$"Admin Mode"  --button=$" use 'su' ":0 \
	--button=$" use sudo ":1 --button=$"Exit":2 \
	--text=$"What method do you use to become Administrator / root?

Note: This is only for the purpose of starting this script.
It does not change anything.
You will be asked later to choose the method you
want to use in the installed system."
	
	ans="$?"
	if [[ $ans -eq 0 ]] ; then
		xterm -fa mono -fs 12 -e su -c "$installer"
	elif [[ $ans -eq 1 ]] ;then
		xterm -fa mono -fs 12 -e  "sudo $installer"
	elif [[ $ans -eq 2 ]]; then
		echo "Good-bye."
		exit 0
	fi

echo "Done."
exit 0

